//---------------------------------------------------------------------------
// Prepar3d - Shader Effect Files
// Copyright (c) 2014, Lockheed Martin Corporation
//---------------------------------------------------------------------------

#include "GPUTerrain.fxh"        
//------------------------------------------------------------------------------
//	Vertex Shader
//------------------------------------------------------------------------------
VS_OUTPUT_HS_INPUT VS(const VS_INPUT v, uint mInstanceID : SV_InstanceID)
{
	VS_OUTPUT_HS_INPUT o = (VS_OUTPUT_HS_INPUT)0; 
 
    const unsigned int leftPoint    = v.vPosition.x;
    const unsigned int thisPoint    = v.vPosition.y;
    const unsigned int rightPoint   = v.vPosition.z;
     
    const int index        = dataBuffer[ v.mInstanceIndex ].x;
    
    const float isCorner    = v.cColor.y;
    const float isNotCenter = v.cColor.x;
    const float isEdge      = v.cColor.z;
    
    GTCItem item = mCache[index];
    GTCItem demItem = mCache[item.DemTextureCoords.z];
    GTCItem parent = mCache[demItem.ParentDemTextureCoords.z];
    o.ActiveChildren = item.ActiveChildren;
	o.MetersPerTexel = (1 << (25 - item.LOD))/64.0f;  // 1 meter per sample at lod 19
    o.DemTex.xy = (item.DemTextureCoords.xy + v.Tex.xy * item.DemTextureCoords.w);
    o.DemTex.z = item.DemTextureCoords.z;
    o.DemTexMip.xy = (item.ParentDemTextureCoords.xy + v.Tex.xy * item.ParentDemTextureCoords.w);
    o.DemTexMip.z = item.ParentDemTextureCoords.z;

    o.TexBase.xy = (item.BaseTextureCoords.xy + v.Tex.xy * item.BaseTextureCoords.w);
    o.TexBase.z = item.BaseTextureCoords.z;

	//o.EmissiveTex.xy = (item.EmissiveTextureCoords.xy + v.Tex.xy * item.EmissiveTextureCoords.w);
    o.TexBase.w = item.EmissiveTextureCoords.z;

    o.DemTexMip.xyz  = lerp(o.DemTex.xyz,o.DemTexMip.xyz,saturate(floor(item.DemTextureCoords.w)));
    const int maxqmic = (1 << 29) - 1;
    const float thisInValid  = saturate(1 - saturate(item.Points[thisPoint].QmicV) + saturate(item.Points[thisPoint].QmicV - maxqmic));
    const float leftInValid  = saturate(1 - saturate(item.Points[leftPoint].QmicV) + saturate(item.Points[leftPoint].QmicV - maxqmic));
    const float rightInValid = saturate(1 - saturate(item.Points[rightPoint].QmicV) + saturate(item.Points[rightPoint].QmicV - maxqmic));
    o.needsBlend = saturate(isNotCenter - saturate(abs(item.AdjacentLods[thisPoint] - index + thisInValid))
                    + isCorner * (2 - saturate(abs(item.AdjacentLods[leftPoint] - index + leftInValid)) 
                                    - saturate(abs(item.AdjacentLods[rightPoint] - index + rightInValid))));

    int3    intOffset      = item.Points[leftPoint].Meters;
    float3  subMeterOffset = item.Points[leftPoint].SubMeters;
    PosNorm pnLeft         = GetPositionAndNormal(intOffset, subMeterOffset);

    intOffset        = item.Points[rightPoint].Meters;
    subMeterOffset   = item.Points[rightPoint].SubMeters;
    PosNorm pnRight  = GetPositionAndNormal(intOffset, subMeterOffset);
    
    PosNorm pnLerp;   
    pnLerp.Position  = lerp(pnLeft.Position,pnRight.Position,0.5);
    pnLerp.Normal    = lerp(pnLeft.Normal,pnRight.Normal,0.5);

    intOffset        = item.Points[thisPoint].Meters;
    subMeterOffset   = item.Points[thisPoint].SubMeters;
    PosNorm pn       = GetPositionAndNormal(intOffset, subMeterOffset);

    const float needsAverage = isEdge * o.needsBlend; 
          float4 vPosition = lerp(pn.Position,pnLerp.Position,needsAverage);
    const float3 normal    = lerp(pn.Normal,pnLerp.Normal,needsAverage);
    const float3 north     = normalize(mul(float4(0,0,1,1), cb_ViewOrigin.TangentMatrix).xyz);
	o.vWorldPos      = vPosition.xyz;
	//o.vLevelMat[1]   = normal;
    //o.vLevelMat[0]   = cross(north,normal);
    //o.vLevelMat[2]   = cross(o.vLevelMat[1], o.vLevelMat[0]);
    o.LevelNorm = normal;
    o.NorthVecWS = north;


    o.DemStitch = int4(demItem.AdjacentLods[8],demItem.AdjacentLods[3],demItem.AdjacentLods[5],demItem.AdjacentLods[4]);
    o.DemStitchParent = int4(parent.AdjacentLods[8],parent.AdjacentLods[3],parent.AdjacentLods[5],parent.AdjacentLods[4]);
	o.DemStitchParent = lerp(o.DemStitch,o.DemStitchParent,saturate(floor(item.DemTextureCoords.w)));
   
    DemLoadData  demLoadData = singleLoadDem(o.DemTex,    o.DemStitch,
                                             o.DemTexMip, o.DemStitchParent,  o.needsBlend);
    
    const float altBlend = demLoadData.Alt;
    vPosition.xyz += altBlend*o.LevelNorm;
    const uint TileSizeMeters = 1 << (24 - item.LOD);  
    // Transform world position with viewprojection matrix
	#if defined(SHD_TERRAIN_SHADOWS)
		float4 vScreenPos;
		float4 ClipRadiusSS;
		o.IsVisible = 0;

		[unroll] for( int cascade = 0; 
						cascade < cb_mNumberCascades; 
						++cascade )
		{
			vScreenPos   = mul( float4( vPosition.xyz, 1.0 ), cb_mGroundViewProjection[cascade] );
            //float4x4 sviewProj = cb_mGroundViewProjection[cascade];
            //sviewProj[3].xyz = float3(0,0,0);
            //ClipRadiusSS.x = length(mul( float4( 2*TileSizeMeters,2*TileSizeMeters,2*TileSizeMeters, 1.0 ), sviewProj) 
            //- (mul( float4( 0,0,0, 1.0 ), sviewProj) ));
            //ClipRadiusSS.xyz = ClipRadiusSS.xxx;
			//ClipRadiusSS.x = cb_mGroundViewProjection[cascade][0][0]*2*TileSizeMeters;
			//ClipRadiusSS.y = cb_mGroundViewProjection[cascade][1][1]*2*TileSizeMeters;
			//ClipRadiusSS.z = cb_mGroundViewProjection[cascade][2][2]*2*TileSizeMeters;
            ClipRadiusSS =  float4( 0,0,0, 1.0 );
			if( vScreenPos.x - ClipRadiusSS.x < vScreenPos.w && vScreenPos.x + ClipRadiusSS.x > -vScreenPos.w &&
				vScreenPos.y - ClipRadiusSS.y < vScreenPos.w  && vScreenPos.y + ClipRadiusSS.y > -vScreenPos.w &&
				vScreenPos.z - ClipRadiusSS.z < vScreenPos.w )
			{
				o.IsVisible = 1;
			}
		}

		o.IsVisible = saturate(ceil(o.IsVisible));
		matrix sceneCameraViewProj = mul( cb_mViewToDevice, cb_mProjToDevice );
		float4 sceneCameraPositionVS =   mul( float4( vPosition.xyz, 1.0 ), sceneCameraViewProj );
        
        // test against shadow draw distance using main camera depth

        // first clip on depth value from main vamera
        const float padMaxDrawRange = cb_mGroundShadowDistance + min(cb_mGroundShadowDistance*0.2,TileSizeMeters);
        //o.IsVisible *= ceil(saturate(padMaxDrawRange - abs(sceneCameraPositionVS.w)));
        // now clip on length of max screespace ray
        const float3 maxSceneRay = float3(cb_mProjToDevice._m00*padMaxDrawRange, cb_mProjToDevice._m11*padMaxDrawRange, padMaxDrawRange);

        const float maxDawDistSq = dot(maxSceneRay,maxSceneRay);
        o.IsVisible *= ceil(saturate(maxDawDistSq - dot(vPosition.xz,vPosition.xz)));
        

		float shadowTessFactorReduction = 1.0f; //Attempt to gain some performance by doing less tessellation on shadow render.
		float depthFactor = abs(sceneCameraPositionVS.w)*(8+58*(1-fTessFactor)*shadowTessFactorReduction); // about 8 pixels per texel at 16  
																	// current range is 4-32 pixels per texel
		// not actually pixels per texel.  This value is actually half the radius of the tile in pixels
		o.PixelsPerTexel.x = TileSizeMeters * max(cb_mProjToDevice._m00*cb_mDisplayParams.x,
												 cb_mProjToDevice._m11*cb_mDisplayParams.y)  
												 / (depthFactor);
	#else
        #if defined(SHD_BATHYMETRY)
            const float radius = TileSizeMeters + 2*(demItem.MaxElevation-demItem.MinBathyElevation);
        #else
            const float radius = 2*TileSizeMeters;//max(TileSizeMeters, 2*(demItem.MaxElevation-demItem.MinElevation));
        #endif
		const matrix viewProj = mul( cb_mViewToDevice, cb_mProjToDevice );
		const float4 vScreenPos =   mul( float4( vPosition.xyz, 1.0 ), viewProj );
		const float2 wPlusRadius = abs(vScreenPos.w) + radius*float2(cb_mProjToDevice._m00,cb_mProjToDevice._m11);
		const float PassesNear = saturate(1 + vScreenPos.w + radius - cb_NearClip);
		const float PassesFar = saturate(1 + cb_FarClip + radius - vScreenPos.w);
		const float PassesU = saturate(1 + wPlusRadius.x - abs(vScreenPos.x));
		const float PassesV = saturate(1 + wPlusRadius.y - abs(vScreenPos.y));
		
		#if defined( SHD_GPU_DEM_SKIP_WATER )
			const float reductionFactor = 3.0f;
        #else
            const float reductionFactor = 1.0f;
		#endif //SHD_GPU_DEM_SKIP_WATER

		const float depthFactor = abs(vScreenPos.w)*(8+58*(1-fTessFactor)*reductionFactor); // about 8 pixels per texel at 16  
																	// current range is 4-32 pixels per texel
		// not actually pixels per texel.  This value is actually half the radius of the tile in pixels
		o.PixelsPerTexel.x = TileSizeMeters * max(cb_mProjToDevice._m00*cb_mDisplayParams.x,
												 cb_mProjToDevice._m11*cb_mDisplayParams.y)  
												 / (depthFactor); 

		o.IsVisible = PassesNear * PassesFar * PassesU * PassesV;
        #if defined(SHD_BATHYMETRY)
                o.IsVisible *= ceil(saturate(50000-dot(vPosition.xz,vPosition.xz)));
        #endif
                //if(item.LOD < 19)  o.IsVisible = 0;
	#endif //SHD_TERRAIN_SHADOWS

    return o; 
}

//------------------------------------------------------------------------------
//	Hull Shader Constant Data
//------------------------------------------------------------------------------
HS_CONSTANT_DATA_OUTPUT ConstantsHS( InputPatch<VS_OUTPUT_HS_INPUT, 9> p, uint PatchID : SV_PrimitiveID )
{
    HS_CONSTANT_DATA_OUTPUT output  = (HS_CONSTANT_DATA_OUTPUT)0;
    
	const uint patchIndex = PatchID % 4;
    const uint4 pIdx = primIdxLookup[patchIndex];
    const float size = p[2].DemTex.x - p[0].DemTex.x;

    const float noChildren = saturate(1-p[0].ActiveChildren[patchIndex]);

    float4 Edges = float4(GetEdgeTessOffset(pIdx[2],pIdx[0]),
                          GetEdgeTessOffset(pIdx[0],pIdx[1]),
                          GetEdgeTessOffset(pIdx[1],pIdx[3]),
                          GetEdgeTessOffset(pIdx[3],pIdx[2]));

    float4 ParentEdges = float4(GetParentEdgeTessOffset(6,0),
                                  GetParentEdgeTessOffset(0,2),
                                  GetParentEdgeTessOffset(2,8),
                                  GetParentEdgeTessOffset(6,8));

    //if(Edges[0] < 32 && Edges[1] < 32 && Edges[2] < 32 && Edges[3] < 32)
    //{
    //    noChildren = 1;
    //}
   
  
	const float DoNotCull =  noChildren * saturate(p[pIdx[0]].IsVisible  +   p[pIdx[1]].IsVisible +
                       p[pIdx[2]].IsVisible +  p[pIdx[3]].IsVisible);



    //if(ParentEdges[0] < 16 && ParentEdges[1] < 16 && ParentEdges[2] < 16 && ParentEdges[3] < 16)
    //{
    //    DoNotCull = 0;
    //}

    const float inside = max(max(Edges[0],Edges[2]), max(Edges[1],Edges[3]));
    const float2 Inside = float2(inside, inside);

    //float2 Inside = float2(max(Edges[0],Edges[2]), max(Edges[1],Edges[3]));

    const float is0 = 1 - saturate(patchIndex);
    const float is1 = 1 - saturate(patchIndex - 1);
    const float is2 = 1 - saturate(patchIndex - 2);
    const float is3 = 1 - saturate(patchIndex - 3);

    Edges[0] = lerp(Edges[0],ParentEdges[0],p[3].needsBlend*(is0+is2));
    Edges[1] = lerp(Edges[1],ParentEdges[1],p[1].needsBlend*(is0+is1));
    Edges[2] = lerp(Edges[2],ParentEdges[2],p[5].needsBlend*(is1+is3));
    Edges[3] = lerp(Edges[3],ParentEdges[3],p[7].needsBlend*(is2+is3));


   // DoNotCull *= size;
    output.Edges[0]  = Edges[0]  * DoNotCull;
    output.Edges[1]  = Edges[1]  * DoNotCull;
    output.Edges[2]  = Edges[2]  * DoNotCull;
    output.Edges[3]  = Edges[3]  * DoNotCull;
    output.Inside[0] = Inside[0] * DoNotCull;
    output.Inside[1] = Inside[1] * DoNotCull;
    return output;
}

//------------------------------------------------------------------------------
//	Hull Shader Dynamic Stage
//------------------------------------------------------------------------------
[domain("quad")]
//[partitioning("fractional_even")]
//[partitioning("integer")]
[partitioning("pow2")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("ConstantsHS")]
[maxtessfactor(TESS_MAX)]  
HS_CONTROL_POINT_OUTPUT HS( InputPatch<VS_OUTPUT_HS_INPUT, 9> inputPatch, 
    uint PatchID : SV_PrimitiveID,  
    uint uInCPID : SV_OutputControlPointID )
{
    const uint patchIndex = PatchID % 4;
    const uint uNeighborCPID = primIdxLookup[patchIndex][(uInCPID + 1) % 4];
	const uint uCPID = primIdxLookup[patchIndex][uInCPID];
    HS_CONTROL_POINT_OUTPUT    output = (HS_CONTROL_POINT_OUTPUT)0;
    // Copy inputs to outputs
    output.vWorldPos = inputPatch[uCPID].vWorldPos;
    output.LevelNorm =   inputPatch[uCPID].LevelNorm;
    output.NorthVecWS =   inputPatch[uCPID].NorthVecWS;
    output.TexBase =  inputPatch[uCPID].TexBase;
    output.DemTex =  inputPatch[uCPID].DemTex;
    output.DemTexMip =  inputPatch[uCPID].DemTexMip;
    output.needsBlend = inputPatch[uCPID].needsBlend;
    output.dropScalar = floor(saturate(inputPatch[uCPID].MetersPerTexel*(0.00064 +0.00064*output.needsBlend)));
    output.DemStitch = inputPatch[uCPID].DemStitch;
    output.DemStitchParent = inputPatch[uCPID].DemStitchParent;
    output.IsLODTenPlus = 1.0f - saturate( saturate(inputPatch[uCPID].MetersPerTexel-510.0f) +
                                           saturate(inputPatch[uCPID].MetersPerTexel-254.0f)*output.needsBlend);
    return output; 
}

//------------------------------------------------------------------------------
//	Domain Shader
//------------------------------------------------------------------------------
//#define LOD_BLEND 1
//#define DEM_SAMPLE 1
#define SIMPLE_LOAD 1 
[domain("quad")]
DS_OUTPUT DS( HS_CONSTANT_DATA_OUTPUT input, float2 BarycentricCoordinates : SV_DomainLocation, 
             const OutputPatch<HS_CONTROL_POINT_OUTPUT, 4> Patch )
{
    DS_OUTPUT output = (DS_OUTPUT)0;
         
	// The barycentric coordinates
    const float U = BarycentricCoordinates.x;
    const float V = BarycentricCoordinates.y;
    const float InvU = 1.0f - U;
    const float InvV = 1.0f - V;

    // u,v : Patch     |    Tri-Strip 
    //---------------  |  ----------
    // 0,0 : Patch[0]  |    0--1
    // 1,0 : Patch[1]  |     /
    // 1,1 : Patch[2]  |    2--3
    // 0,1 : Patch[3]  |

    const float weights[4] = {  InvU * InvV,   U * InvV, 
                                InvU * V,      U * V      };

    const float3 DemTex = float3(  lerp(Patch[0].DemTex.x,Patch[3].DemTex.x,U),
                                   lerp(Patch[0].DemTex.y,Patch[3].DemTex.y,V),
                                   Patch[0].DemTex.z );

    const float3 DemTexMip = float3(  lerp(Patch[0].DemTexMip.x,Patch[3].DemTexMip.x,U),
                                      lerp(Patch[0].DemTexMip.y,Patch[3].DemTexMip.y,V),
                                      Patch[0].DemTexMip.z );

    const float blend = Patch[0].needsBlend * weights[0]
                     +  Patch[1].needsBlend * weights[1]
		             +  Patch[2].needsBlend * weights[2]
                     +  Patch[3].needsBlend * weights[3];

	float3 vWorldPos =  Patch[0].vWorldPos * weights[0]
                 +  Patch[1].vWorldPos * weights[1]
                 +  Patch[2].vWorldPos * weights[2]
                 +  Patch[3].vWorldPos * weights[3];

   const float3 normal =  Patch[0].LevelNorm * weights[0]
				 +  Patch[1].LevelNorm * weights[1]
				 +  Patch[2].LevelNorm * weights[2]
				 +  Patch[3].LevelNorm * weights[3];
   const float3 fixedNormal = normalize(normal);

   


    float altBlend = 0;

    DemLoadData  demLoadData = singleLoadDem(DemTex,
                                             Patch[0].DemStitch,
                                             DemTexMip,
                                             Patch[0].DemStitchParent,
                                             blend);


  // altBlend = demLoadData.BathyAlt;
    altBlend = demLoadData.Alt;

    const float dropScalar = Patch[0].dropScalar * weights[0]
                          +  Patch[1].dropScalar * weights[1]
		                  +  Patch[2].dropScalar * weights[2]
                          +  Patch[3].dropScalar * weights[3];

    const float curveOffset = dropScalar * abs(length(fixedNormal-normal)*6371000);

	//#if defined(SHD_TERRAIN_SHADOWS) && defined(SHD_WATER_DETAIL_4)
	//	altBlend -= demLoadData.IsWater*1000; 
	//#endif
        
	#if defined(SHD_GPU_DEM_SKIP_WATER)

	    altBlend = lerp(altBlend,min(cb_SurfaceAlt - 1,altBlend),demLoadData.IsWater);
		
        output.fClipDistance = altBlend - cb_SurfaceAlt + 2;
        output.fCullDistance = output.fClipDistance;
	#endif
    #if defined(SHD_BATHYMETRY) || defined(SHD_TERRAIN_SHADOWS)
        float3 bathyPos = vWorldPos + min(demLoadData.Alt, demLoadData.BathyAlt)*fixedNormal;
        #if defined(SHD_BATHYMETRY) && !defined(SHD_TERRAIN_SHADOWS)
            output.vBathyPosWS = bathyPos; 
        #endif
    #endif

    vWorldPos += (altBlend+curveOffset)*fixedNormal;

	#if defined(SHD_TERRAIN_SHADOWS)
       // vWorldPos = lerp(vWorldPos,bathyPos,demLoadData.IsWater);
		output.vPosition = float4( vWorldPos, 1.0 );  vWorldPos;//mul(float4( vWorldPos, 1.0 ),cb_mWorld);
	#else
        
        float3x3 level;
        level[0] = cross(Patch[0].NorthVecWS,fixedNormal); 
        level[1] = fixedNormal; 
        level[2] = cross(level[1],level[0]); 
        output.vNormal = mul(demLoadData.Normal,level);
        // take cross product of dem normal and x or z component of level matrix to get normal/binormal and pass
        // those to the ps instead of the level matrix.  the tangent can be calculated in place
        #if defined (SHD_HAS_LAND)
        output.Specular = demLoadData.Specular;
        #endif
        output.LandClassTemp = demLoadData.LandClassTemp;
       // output.Radiosity = demLoadData.Radiosity; 
      //  output.DoBathy = 0.0f;

        output.TexBase.x = lerp(Patch[0].TexBase.x,Patch[3].TexBase.x,U);
        output.TexBase.y = lerp(Patch[0].TexBase.y,Patch[3].TexBase.y,V);
        output.TexBase.z = Patch[0].TexBase.z;
        output.TexBase.w = Patch[0].TexBase.w;
        #if defined (SHD_HAS_WATER)
 
        output.LevelNorm = level[1]; 
        output.LevelBiNorm    = level[0]; 
        output.IsWater = saturate(demLoadData.IsWater);
        #endif
		// Transform world position with viewprojection matrix
		matrix viewProj = cb_mViewProj;

		output.vPosition =   mul( float4( vWorldPos, 1.0 ), viewProj );    
		output.vPosition.z = LinearizeDepthZ(output.vPosition);
       // output.vDepth = output.vPosition.z;
		#if !defined(SHD_GPU_DEM_SKIP_WATER) && defined(SHD_HAS_WATER)

			//#if defined(SHD_GPU_DEM_WATER_REFLECTION)
			//	output.TexEnvMap.xyz = output.vPosition.xyz + output.vPosition.www;
			//	output.TexEnvMap.w = 2.0 * output.vPosition.w;
			//#endif // SHD_GPU_DEM_WATER_REFLECTION
            float isWater = saturate(demLoadData.IsWater);
            output.Shoreline = 1-saturate((demLoadData.Alt - demLoadData.BathyAlt) / max(fMaxWaveHeight*0.5,3.0f));
            output.vFoamScale = FoamScale[demLoadData.IsWater]*Patch[0].IsLODTenPlus; 
			#if defined(SHD_WATER_DETAIL_4)
    
				const float dist = dot( vWorldPos.xyz, vWorldPos.xyz );

				float4 TexBumpDetail = float4(ComputeDetailTextureCoordinates(vWorldPos,  16),1,1);

					//  output.TexBump1 = 0.075*output.TexBumpDetail.yxzw;

                 [branch]if(//dist < 10000000 && 
                    isWater > 0)
                {
                    
                    float shorelineDampen = saturate((demLoadData.Alt - demLoadData.BathyAlt) / fMaxWaveHeight);
                    
                    
                    if(isWater > 0.75)
                    {
		                TexBumpDetail.y *= -1.0;
                        
                        float3 waterOffset = txWaterDisplacement.SampleLevel(samWrap, TexBumpDetail.xy, 0).xyz;
                        waterOffset.xy *= -1;
			            vWorldPos.xyz += mul(waterOffset.xzy, level)*shorelineDampen;
		            }
	            }

				output.vPosition =   mul( float4( vWorldPos, 1.0 ), viewProj );
			#endif //SHD_WATER_DETAIL_4
		#endif //SHD_GPU_DEM_SKIP_WATER
	    output.vPosWS = vWorldPos;
	#endif //SHD_TERRAIN_SHADOWS

    return output;
}

#if defined(SHD_TERRAIN_SHADOWS)
	struct GS_OUTPUT
	{
		float4 vPosition    : SV_POSITION;     // Projection coord
		uint   RTIndex      : SV_RenderTargetArrayIndex;
	};
	struct SHADOW_PS_INPUT
	{
		float4 vPosition    : SV_POSITION;     // Projection coord
	};
    [instance(SHD_GS_INSTANCE_COUNT)]
	[maxvertexcount(3)]
	void GS( triangle DS_OUTPUT input[3], inout TriangleStream<GS_OUTPUT> MeshStream, uint InstanceID : SV_GSInstanceID )
	{
		// Compute screen coordinates
		GS_OUTPUT output[3];
        [unroll] for( int v = 0; v < 3; v++ )
        {
            output[v].RTIndex = InstanceID;
            output[v].vPosition = mul( input[v].vPosition, cb_mGroundViewProjection[InstanceID] );
		    output[v].vPosition.z = max(output[v].vPosition.z,0.0);
            // first clip on depth value from main vamera
            const float padMaxDrawRange = cb_mGroundShadowDistance * 1.15;
           // output[v].vPosition.z +=  1 - ceil(saturate(padMaxDrawRange - abs(output[v].vPosition.z)));
            // now clip on length of max screespace ray
            const float3 maxSceneRay = float3(cb_mProjToDevice._m00*padMaxDrawRange,
                                  cb_mProjToDevice._m11*padMaxDrawRange,
                                  padMaxDrawRange);
            const float maxDawDistSq = dot(maxSceneRay,maxSceneRay);
            output[v].vPosition.z += 1 - ceil(saturate(maxDawDistSq - dot(input[v].vPosition.xz,input[v].vPosition.xz)));
        }
        //const float2 v1 = output[1].vPosition.xy - output[0].vPosition.xy;
        //const float2 v2 = output[2].vPosition.xy - output[0].vPosition.xy;
        //if( v1.x*v2.y - v1.y*v2.x <= 0.1 )
        {
            MeshStream.Append( output[0] );
            MeshStream.Append( output[1] );
            MeshStream.Append( output[2] );
            MeshStream.RestartStrip();
        }
	}
#endif //SHD_TERRAIN_SHADOWS

#if defined(SHD_TERRAIN_SHADOWS)
	#if USE_VARIANCE_SHADOWS==1
		float2 PS(SHADOW_PS_INPUT Input) : SV_TARGET
		{
			float2 depthValues = float2(0.0f,0.0f);

			depthValues.x = Input.vPosition.z;
			depthValues.y = depthValues.x * depthValues.x;

			//M2 = z^2 + (1/4) * (ddx(z)^2 + ddy(z)^2);
			const float depthDDX = ddx(Input.vPosition.z);
			const float depthDDY = ddy(Input.vPosition.z);
			depthValues.y += (0.25 * ((depthDDX*depthDDX)+(depthDDY*depthDDY)));

			return depthValues;
		}
	#else
		float PS(SHADOW_PS_INPUT Input) : SV_Depth
		{
			return Input.vPosition.z;
		}
	#endif 
#else  
#if defined(SHD_BATHYMETRY)
    [instance(2)]
	[maxvertexcount(6)]
	void GS( triangle DS_OUTPUT input[3], inout TriangleStream<DS_OUTPUT> MeshStream,
                                          uint InstanceID : SV_GSInstanceID)
	{
		// Compute screen coordinates
		DS_OUTPUT output[3];
        int v;
        [branch] if( input[0].IsWater + input[1].IsWater + input[2].IsWater > 0 ) 
        {
            float3 inRange = float3(saturate(25600 - dot(input[0].vPosWS.xz,input[0].vPosWS.xz)),
                                    saturate(25600 - dot(input[1].vPosWS.xz,input[1].vPosWS.xz)),
                                    saturate(25600 - dot(input[2].vPosWS.xz,input[2].vPosWS.xz)));
            float anyInRange = inRange.x + inRange.y + inRange.z;
            [branch] if( input[0].vPosWS.y > -25 && anyInRange > 0.0f ) 
            {
                [branch]if(InstanceID == 0)
                {
                    [unroll] for( v = 0; v < 3; ++v )
			        {
                        output[v] = input[2-v];
			        }
                }
                else
                {
                    [unroll] for( v = 0; v < 3; ++v )
			        {
                        output[v] = input[v];
                        output[v].IsWater = -2.0f; // will force skipping water shading in ps
                        output[v].vPosWS = lerp(output[v].vPosWS,output[v].vBathyPosWS,saturate(inRange[v] - 1.0f + input[v].IsWater ));   
                        output[v].vPosition = mul( float4( output[v].vPosWS, 1.0 ), cb_mViewProj );    
                        output[v].vPosition.z += 0.25*saturate(1-saturate(output[v].vPosWS.y));
			        }
                }
                //const float2 ss0 = output[0].vPosition.xy/output[0].vPosition.w;
                //const float2 ss1 = output[1].vPosition.xy/output[1].vPosition.w;
                //const float2 ss2 = output[2].vPosition.xy/output[2].vPosition.w;
                //const float2 v1 = ss1 - ss0;
                //const float2 v2 = ss2 - ss0;
                //if( v1.x*v2.y - v1.y*v2.x < 0.2f )
                {
                    MeshStream.Append( output[0] );
                    MeshStream.Append( output[1] );
                    MeshStream.Append( output[2] );
                    MeshStream.RestartStrip();
                }
            }
        }
	}
#endif //SHD_TERRAIN_SHADOWS

    float4 PSt(PS_INPUT Input) : SV_TARGET
    {
	    float4 temp = Input.vPosition + Input.TexBase ;
        temp.xyz += Input.vPosWS + Input.vNormal;
    #if defined(SHD_HAS_LAND)
        temp.xyz += Input.Specular;
    #endif
    #if defined(SHD_HAS_WATER)
        temp.xyz +=  Input.LevelNorm + Input.LevelBiNorm;
        temp.x += Input.IsWater + Input.Shoreline;
    #endif
        temp.x += Input.LandClassTemp;
        return saturate((temp.x - temp.y + temp.z - temp.w) * 0.75 * temp);
    }
 
    float4 PSn(PS_INPUT Input) : SV_TARGET
    {
        return txBase.Sample(samClamp, Input.TexBase.xyz);
	    //return float4(0,1,1,1);
    }



	float4 PS(PS_INPUT Input) : SV_TARGET
	{ 
		//Get the base color.   
		float4 BaseColor = txBase.Sample(samClamp, Input.TexBase.xyz);

		const float fAlpha = saturate(BaseColor.a);

        // Caluclate and compute geometry data.
        #if defined(SHD_GDATA)
            //ObjectType lines up with our renderable object types and general material.
            //4 is terrain, 8 is water. Lerp equation x*(1-s) + y*s.
            uint objectType = (uint)lerp(8,4,saturate(floor(fAlpha * 2)));
            return ComputeGeometryData(BaseColor,objectType,Input.vPosWS,Input.vNormal);
        #endif//SHD_GDATA

		//Get vector half normals for calculating specular.
		const float3 vEyeVectWS = -Input.vPosWS;
		const float eyeDist = abs(length(vEyeVectWS));

		#if defined(SHD_LAND_DETAIL) && defined(SHD_HAS_LAND)
            float2 LandDetail = (Input.vPosWS.xz + cb_DetailTextureOrigin) * 64.0f * 1 * 0.000390625 + (float2(1, 1) * 0.003f);
			float4 landDetailPartialDerivative = float4(ddx(LandDetail),ddy(LandDetail));
			[branch]if(eyeDist < 250) //This may not be worth the branch overhead cost, depends on what is slowing down the terrain.
			{
				float detailBlend =  1 - saturate((eyeDist - 50)*0.005);
				float LandDetailScalar = txLandDetail.SampleGrad(samWrap, float3(LandDetail,0),landDetailPartialDerivative.xy,landDetailPartialDerivative.zw).x;
				LandDetailScalar = lerp(1,2.0f * LandDetailScalar,.5f*fAlpha*detailBlend);
				BaseColor *= LandDetailScalar;
			}
		#endif //SHD_LAND_DETAIL
	    float4 debugColor = float4(1,0,0,1);
	    float4 FinalColor = BaseColor;
	    float4 ReflectionColor = BaseColor;
	    float4 RefractionColor = BaseColor;
	    float4 EnvironmentColor = float4( 0, 0, 0, 1 );
	    float3 EmissiveColor = txEmissive.Sample(samClamp, Input.TexBase.xyw).xyz;   //Input.Emissive;
        EmissiveColor = (EmissiveColor*EmissiveColor);
        float foam = 0.0f;
        #if defined(SHD_HDR)
        EmissiveColor *= 0.35f;
        #endif
		//Landclass
		float LandClassTemp = 0.5;

		//Sun/moon values
		const float3 sunDiffuse = cb_mLights[SUN_LIGHT].mDiffuse.xyz;
		const float3 sunAmbient = cb_mLights[SUN_LIGHT].mAmbient.xyz;//*(-0.1f*Input.Radiosity + 0.9);
		const float3 sunDirection = cb_mLights[SUN_LIGHT].mDirection;
		const float3 moonDiffuse = cb_mLights[MOON_LIGHT].mDiffuse.xyz;
		const float3 moonAmbient = cb_mLights[MOON_LIGHT].mAmbient.xyz;//(-0.1f*Input.Radiosity + 0.9);
		const float3 moonDirection = cb_mLights[MOON_LIGHT].mDirection;

		//Shadow calculation variables.
		float3 ShadowPositionWS = Input.vPosWS;
		float shadowContrib = 1.0;

		//Light calculation variables.
		float3 runningNormal = float3( -Input.vNormal.r, Input.vNormal.g, Input.vNormal.b );
        
		float3 FinalLighting = float3(0.0f, 0.0f, 0.0f);
		float2 specularFactor = float2(0.0f, 0.0f);

        #if defined (SHD_HAS_LAND)
		float specularBoost = Input.Specular.x;
		float specularPower = max( Input.Specular.z, 3.5f );
        float specularBlend = min( Input.Specular.y, 1.0f / specularPower );
        #endif
		//Reflection fresnal value
		float reflectionFresnel = 1;

		float3 vEyeDirWS = (vEyeVectWS) / eyeDist; //Same as normalize.
		float3 vreflect = 2.0 * dot(runningNormal, sunDirection) * runningNormal - sunDirection;

  		float3 vHN = saturate(normalize(vEyeDirWS.xyz + sunDirection));
		//vHN = vreflect;
		float3 vHN2 = saturate(normalize(vEyeDirWS.xyz + moonDirection));
		// Only compute the specular factor if IR is not active
		#if !defined(SHD_IR_ACTIVE) && defined(SHD_HAS_LAND)
		
			specularFactor = specularBlend *
				saturate((pow(abs(specularBoost * saturate(float2(dot(vreflect,vEyeDirWS.xyz), dot(runningNormal, vHN2))))
				, specularPower)));
        #endif//!SHD_IR_ACTIVE

        #if defined(SHD_HAS_WATER) && !defined(SHD_GPU_DEM_SKIP_WATER)

			const float bias = 1 + 3 * saturate( 1.0f - dot( vEyeDirWS,float3(  0, 1, 0 )));

            const float3 vWorldPos = Input.vPosWS;
			//Seems to be broken due to cb_mWorld being invalid.
            const float windFactor = 0.9 + (pow(fWindSpeed/32.0f,0.5f) * 0.2f);

            #if defined(SHD_WATER_DETAIL_4)
            float4 TexBumpDetail = float4(ComputeDetailTextureCoordinates(vWorldPos,  16),1,1);
			TexBumpDetail.y *= -1;
            #endif //SHD_WATER_DETAIL_1

		    //Terrible but can't calculate partial derivatives in branch.
            #if defined(SHD_WATER_DETAIL_1)

				const float4 TBump5 = ComputeWaterTextureCoordinates( vWorldPos, 4 );	
				const float4 BPD5 = bias * float4(ddx(TBump5.xy),ddy(TBump5.xy));

                #if defined(SHD_WATER_DETAIL_4)
                    const float4 TritonBPD = bias * float4(ddx(TexBumpDetail.xy),ddy(TexBumpDetail.xy));
                #endif //SHD_WATER_DETAIL_1
            #endif

        #endif 

		#if defined(SHD_GPU_DEM_WATER_REFLECTION) 

  			float2 BumpEnv = cb_mDisplayParams.zw * Input.vPosition.xy * 0.5;

            BumpEnv.y = 1.0f - BumpEnv.y;
            // eye distance over 1500 meteres clamped to 8
			const float4 envPartialDerivative = min( eyeDist * 0.00066666666, 8 ) * float4(ddx(BumpEnv),ddy(BumpEnv)); 

		#endif
        #if defined(SHD_HAS_WATER)
		#if defined(SHD_GPU_DEM_SKIP_WATER)
			[branch]if(Input.IsWater > 0.9999)
			{
				discard;
			}
		#else
            #if defined(SHD_HAS_LAND) || defined(SHD_BATHYMETRY)
            //Branch if not a solid piece of land.
			[branch]if((fAlpha - Input.IsWater) < 1.0f)
			{
            #endif
				// Determine the detail bump blend (which is based on the eye distance)
				float DetailBumpBlend = pow(saturate(1.0f-(eyeDist/250.0f)),2).x;

		        // Sample Detail bump
		        float3 tmpBump;
		        float3 Bump = float3( 0, 0, 0.5 );

                float sumOfBlends = 0;
			    #if defined(SHD_WATER_DETAIL_1)
                    float4 TBump;
                    float4 BPD;
                    
                    #if defined(SHD_WATER_DETAIL_3) && !defined( SHD_WATER_DETAIL_4 )
                        BPD = BPD5 * cb_WaterTextureData[0].TextureScale;
			            TBump = ComputeWaterTextureCoordinates( vWorldPos, 0);
                        if(TBump.z > 0)
                        {
					        Bump += TBump.z * (txBumpOne.SampleGrad( samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw).xyz -  0.5);
                            sumOfBlends += TBump.z;
                        }
                    #endif
                    #if defined(SHD_WATER_DETAIL_2)
                        BPD = BPD5 * cb_WaterTextureData[1].TextureScale;
			            TBump = ComputeWaterTextureCoordinates( vWorldPos, 1 );
                        if(TBump.z > 0)
                        {
                            Bump += TBump.z * (txBumpTwo.SampleGrad( samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw).xyz -  0.5);
                            sumOfBlends += TBump.z;
                        }
                    #endif
                    #if defined(SHD_WATER_DETAIL_3) && !defined( SHD_WATER_DETAIL_4 )
				        BPD = BPD5 * cb_WaterTextureData[2].TextureScale;
                        TBump = ComputeWaterTextureCoordinates( vWorldPos, 2 );	
                        if(TBump.z > 0)
                        {                    
                            Bump += TBump.z * (txBumpThree.SampleGrad( samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw).xyz - 0.5);
                            sumOfBlends += TBump.z;
                        }
                    #endif
                    #if defined(SHD_WATER_DETAIL_2)
				        BPD = BPD5 * cb_WaterTextureData[3].TextureScale;
				        TBump = ComputeWaterTextureCoordinates( vWorldPos, 3 );	                        
                        if(TBump.z > 0)
                        {
                            Bump += TBump.z * (txBumpFour.SampleGrad( samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw).xyz - 0.5);
                            sumOfBlends += TBump.z;
                        }
                    #endif
                    #if defined(SHD_WATER_DETAIL_1)
                        TBump.z = TBump5.z * saturate((1.15 - saturate(TBump5.z - 0.95)*20));
                        if(TBump.z > 0)
                        {
					        Bump += TBump.z *  (txBumpFive.SampleGrad( samBump, float3( TBump5.xy, 0 ), BPD5.xy, BPD5.zw).xyz - 0.5);
                            sumOfBlends += TBump.z;
                        }
                         
                    #endif
                   // float4 texNoise = float4(ComputeDetailTextureCoordinates(vWorldPos,0.125),1,1);    
                   // float noise = txNoise.SampleGrad( samBump, float3( texNoise.xy, 0 ), BPD5.xy*0.125, BPD5.zw*0.125).x;
                    float foamVal = 0;
                    Bump -= float3(0,0,0.5)*saturate(sumOfBlends);
                    Bump = normalize(Bump);
                    #if defined(SHD_WATER_DETAIL_3) && !defined(SHD_WATER_DETAIL_4)
                    {
                        TBump = ComputeWaterTextureCoordinates( vWorldPos, 0 );
                        foamVal += saturate( Input.vFoamScale * 0.1 * saturate( Bump.y*2 ) * (1-fAlpha)*(1-eyeDist*0.001) );
				        [branch]if(eyeDist < 500)
				        {
                            foamVal *= lerp(1,saturate(txFoam.SampleGrad(samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw ).x),saturate(1-eyeDist*0.002));
                        }
                    }
                    #endif                        
                #endif

			    #if defined(SHD_WATER_DETAIL_4)
				    [branch]if(eyeDist < 2500)
				    {
				        tmpBump.xyz = txWaterSloapFoam.SampleGrad(samBump, TexBumpDetail.xy,TritonBPD.xy,TritonBPD.zw).xyz;
                        TBump = ComputeWaterTextureCoordinates( vWorldPos, 0);
                        const float blendBump = saturate((eyeDist-500)*0.0005);
                        foamVal += saturate( Input.vFoamScale * 0.1 * saturate( tmpBump.z ) * (1-fAlpha)*(1-eyeDist*0.001) );
				        [branch]if(eyeDist < 500)
				        {
                            foamVal *= lerp(1,saturate(txFoam.SampleGrad(samBump, float3( TBump.xy, 0 ), BPD.xy, BPD.zw ).x),saturate(1-eyeDist*0.002));
                        }
					    float3 sx = float3(1.0f, -tmpBump.x, 0.0f);
					    float3 sy = float3(0.0f, -tmpBump.y, 1.0f);
					    tmpBump.xzy = normalize(cross( sy,sx));
					    Bump.xyz = normalize( float3( tmpBump.xyz * (1 - blendBump)) + Bump.xyz*(0.4 + 0.6*blendBump) ); 

				    }
			    #endif

                    #if defined(SHD_WATER_DETAIL_1)
                        BaseColor += foamVal;
                    #endif

				    //Get the specular settings out of the water. Hopefully we can do this per dem point at some point.
				    //Lerp between the values for the coast lines.
				    const float3 unLeveledBump = Bump.xyz;

                    const float3x3 level = { Input.LevelBiNorm, Input.LevelNorm, cross( Input.LevelNorm, Input.LevelBiNorm ) };

				    Bump.xyz = normalize( mul( Bump.xzy, level ));
                    
					#if defined(SHD_HAS_LAND)
                    specularBlend = cb_SpecularBlend; 
				    specularPower = cb_SpecularPower;
				    specularBoost = cb_SpecularBoost;
				    runningNormal = lerp(Bump.xyz,runningNormal,fAlpha);
                    #else
                    const float specularBlend = cb_SpecularBlend;
				    const float specularPower = cb_SpecularPower;
				    const float specularBoost = cb_SpecularBoost;
                    runningNormal = Bump.xyz;
                    #endif 
                     
				    vreflect = 2.0 * dot(runningNormal,sunDirection) * runningNormal - sunDirection;
                       
                    #if defined(SHD_HAS_LAND)
				    specularFactor = (specularBlend *
					    (pow(abs(specularBoost * saturate(float2(dot(vreflect,vEyeDirWS.xyz), dot(Bump.xyz, vHN2))))
					    , specularPower)) * (1-fAlpha));
                    #else
                    specularFactor = (specularBlend *
					    (pow(abs(specularBoost * saturate(float2(dot(vreflect,vEyeDirWS.xyz), dot(Bump.xyz, vHN2))))
					    , specularPower)));
                    #endif
                    #if defined(SHD_WATER_DETAIL_1)
                    specularFactor *= (1-foamVal);
                    #endif
				    //This fresnel takes into account the difference in reflection and fraction based on the viewing angle.
				    //reflectionFresnel = 1.0f - max(dot(vEyeDirWS,runningNormal) * 1.3f,0); //Cheap way.
				    //More physically accurate but doesn't look that good for us. Might with refration turned on.
					float3 fresnelNormal = lerp( runningNormal.xyz, float3( 0, 1, 0 ), saturate( .35f + ( eyeDist / 40000 )));
				    reflectionFresnel = clamp( .001f + 0.99f * pow( saturate(1 - dot( vEyeDirWS.xyz, fresnelNormal)), 4 ), 0, 1 );
                    //return float4( reflectionFresnel, reflectionFresnel, reflectionFresnel, 1.0f );
					
				    // Sample the environment texture
			    #if defined(SHD_GPU_DEM_WATER_REFLECTION )
				    BumpEnv += ( 0.06f * unLeveledBump.xy );
				    ReflectionColor = max( txEnv.SampleGrad(samClamp, float3(BumpEnv,0), envPartialDerivative.xy,envPartialDerivative.zw), .5f * cb_mFogColor );
                #else
                    ReflectionColor.xyz = cb_mFogColor.xyz;//((saturate( vreflect * sunDiffuse ) + saturate( vHN2 * moonDiffuse )) ) * BaseColor.rgb * 2;
			    #endif //SHD_GPU_DEM_WATER_REFLECTION

			    #if defined(SHD_GPU_DEM_WATER_REFRACTION)
				    // sample refraction texture
				    //float2 BumpRef = (Input.TexEnvMap.xy/Input.TexEnvMap.w) + ( 0.075f * unLeveledBump.xy );
				    //RefractionColor = txRefraction.SampleGrad( samClamp, float3( BumpRef.x, 1-BumpRef.y,0), envPartialDerivative.xy,envPartialDerivative.zw);    
                #else
                    RefractionColor.xyz *= ( saturate( dot( float3( vreflect.x, -vreflect.y, vreflect.z ), sunDirection.xyz ) * sunDiffuse ) + saturate( vHN2 * moonDiffuse ));
			    #endif //SHD_GPU_DEM_WATER_REFRACTION
         
				//Calculate the environmental color based on the reflection and refraction colors.
				//EnvironmentColor = float4(lerp(lerp(ReflectionColor, RefractionColor, 0.5f), BaseColor, reflectionFresnel * saturate(0.6f*fAlpha+0.4f)));
				//This handles blending between the reflection and refraction based on how you're viewing the waters surface.
				//The night interpolant is used to darken the water at night. Since later the diffuse lighting has been removed to resolve reflection issues.
				float4 reflectionRefractionColor = (1-cb_mDayNightInterpolant) * RefractionColor * (1.0f - reflectionFresnel) + ReflectionColor * reflectionFresnel;

				//Environmental color takes into account the reflection and refraction color. Assuming most things in this are already lite.
				EnvironmentColor.rgb = .40f * reflectionRefractionColor.rgb * ( 1 - fAlpha );//float4(lerp(reflectionRefractionColor, BaseColor, (1-cb_mDayNightInterpolant) * fAlpha));
                BaseColor *= lerp( .42f, 1.0f, fAlpha );
                //BaseColor.xyz += 0.5*foam*(1-fAlpha);
                #if defined(SHD_HAS_LAND)
				//Blend between the black and emissive based on alpha.
				EmissiveColor = lerp(float3(0.0f,0.0f,0.0f),EmissiveColor, fAlpha);
				//Adjust the texShadows.
				ShadowPositionWS += (1-fAlpha)*0.45f * float3(Bump.xy,0);
                #else
				//Blend between the black and emissive based on alpha.
				EmissiveColor *= 0.4;
				//Adjust the texShadows.
				ShadowPositionWS += 0.45f * float3(Bump.xy,0);
                #endif
            #if defined(SHD_HAS_LAND)
			}
            #endif
		#endif //SHD_GPU_DEM_SKIP_WATER
        #endif
		#if defined(SHD_RECEIVE_SHADOWS) && !defined(SHD_GPU_DEM_SKIP_WATER)
            shadowContrib = ShadowContribution(Input.vPosition.w,ShadowPositionWS);
		#endif //SHD_RECEIVE_SHADOWS

       // shadowContrib=1;

		//Handle all point light lighting. Pixel shader bound!
		//float3 pointLightsDiffuse =	0.0f;//AccumulatePointLights(Input.vPosWS, runningNormal) * shadowContrib;

		//Get the angle of contribution for moon and sun based on the current normal.
		const float sunContrib = saturate(dot(runningNormal, normalize(sunDirection)));
		const float moonContrib = saturate(dot(runningNormal, normalize(moonDirection)));

		//Calculate the final specular color.
		const float3 finalSpecularSun = saturate(specularFactor.x * sunDiffuse);
		const float3 finalSpecularMoon = saturate(specularFactor.y * moonDiffuse);
		const float3 finalSpecular = (finalSpecularSun + finalSpecularMoon) * shadowContrib;
   
		//Calculate the final ambient / diffuse coloring for the sun and moon.
		const float3 finalSunColor = (sunAmbient + saturate(sunDiffuse * (sunContrib * shadowContrib)));// + pointLightsDiffuse);
		const float3 finalMoonColor = (moonAmbient + saturate(moonDiffuse * (moonContrib * shadowContrib)));// + pointLightsDiffuse);
		const float3 finalAmbientDiffuse =  finalSunColor + finalMoonColor;
		   
		//Do final diffuse and ambient on the base color. Note: Ambient should be applied later probably.
                float4 unlitBaseColor = BaseColor;
		BaseColor.xyz = BaseColor.xyz * finalAmbientDiffuse;

		//Lerps are to handle the different lighting based on water or not water.
		EnvironmentColor.xyz = BaseColor.xyz + EnvironmentColor.xyz;// lerp( lerp( BaseColor.xyz,EnvironmentColor.xyz, 1.0f - fAlpha ), BaseColor.xyz, fAlpha );
	    
		//Take into account the dynamic direction lights on the environmental color.
		FinalLighting = EnvironmentColor.xyz + finalSpecular;

		//Add in the emissive color based on the day night interpolant.
		FinalLighting = (EmissiveColor * cb_mDayNightInterpolant) + FinalLighting;

		//Work in progress.
		FinalColor = float4(FinalLighting, fAlpha);
        //FinalColor *= ceil(saturate(0.45-max(abs(Input.TexBase.x - 0.5),abs(Input.TexBase.y - 0.5))));
        //if(FinalColor.r == 0)
        //{
        //    #ifdef SHD_HAS_LAND
        //       FinalColor.g = 1;
        //    #endif
        //    #ifdef SHD_HAS_WATER
        //       FinalColor.b = 1;
        //    #endif
        //}
		// Apply IR if active
		#if defined(SHD_IR_ACTIVE)
			FinalColor = ApplyIrGpuTerrain(unlitBaseColor, Input.LandClassTemp, FinalColor.rgb);
			return FogIR(FinalColor, eyeDist);
        #endif

#if defined(SHD_BATHYMETRY)
        float3 fogColor = float3(0.35f,0.6f,0.8f)*(cb_mCloudAmbient.xyz);
        FinalColor = float4(FogPS(FinalColor.xyz, eyeDist, 0.000328402053f, fogColor), FinalColor.a);
#else
		//Compute fog contribution.
        #if defined(SHD_VOLUMETRIC_FOG)
            #if defined(SHD_GPU_DEM_SKIP_WATER)
                FinalColor = VolumetricFogPS(Input.vPosWS.y, FinalColor, eyeDist - ( 2.0f * cb_Altitude - cb_SurfaceAlt ),
                    cb_mFogDensity, cb_mFogColor.xyz, float3(1,1,1));
            #else
                 FinalColor = VolumetricFogPS(Input.vPosWS.y, FinalColor, eyeDist, cb_mFogDensity, cb_mFogColor.xyz, float3(1,1,1));
            #endif
        #else
			FinalColor = float4(FogPS(FinalColor.xyz, eyeDist, cb_mFogDensity, cb_mFogColor.xyz), FinalColor.a);
        #endif //SHD_VOLUMETRIC_FOG 
#endif
            // ---- Visualize Water Class -----
            //float wc = Input.LandClassTemp; 
            //float wcvalue = fmod(wc,6.0f)/6.0f;
            ////FinalColor.rgb = float3(0,0,0);
            //if(wc <= 0.5f) FinalColor.r = 2*(wc);;
            //if(wc > 0.5f) FinalColor.rg = 2*(wc-0.5); 

            // ---- Visualize Foam Scale -----
            #if defined(SHD_HAS_WATER)
                            //FinalColor.rg = Input.Shoreline;
                            //FinalColor.g = Input.vFoamScale/5;
                            //FinalColor.b = 0;
            #endif
            // ---- Visualize LandClass ---- // requires header change 
            //FinalColor.rgb = Input.LandClassTemp; 
            
            // ---- Visualize Normal -----
            //FinalColor.xyz = (normalize(runningNormal.xyz)+1)*0.5;
            //FinalColor.y = 0;
        //FinalColor.rgb = BaseColor.rgb - int3(BaseColor.rgb);
            // ----- Visualize Base Texture UVs ------
          //  FinalColor.r = Input.TexBase.x;
          //  FinalColor.b = fAlpha;
          //  FinalColor.g=shadowContrib;
		#if SHOW_CASCADES>=1
			int index = (int)shadowContrib;
			float4 cascadeColor = CascadeColorsMultiplier[index];
			return cascadeColor;
		#else
            DEBUG_MARK(FinalColor)
            int test = 255;
			return FinalColor;
		#endif
	}
#endif //SHD_TERRAIN_SHADOWS