## Intro ##
These are the HDR tweak files per Mike Lacovetta found at http://forum.avsim.net/topic/465890-p3d-hdr-brightness-mod-bright-daytime-and-dark-night/. I am not the author. I created this git based on his findings for convenience only.

## To download the tweaks... ##
Just head-on over to https://bitbucket.org/pnirattisai/p3d-hdr-tweaks/downloads#tag-downloads and download the zip of "Mike_Lacovetta's_tweaks". You can also find the original unmodified files at the same link.

## Using ##
The two tweaked files are:

/Prepar3D/ShadersHLSL/GPUTerrain.fx

and

/Prepar3D/ShadersHLSL/PostProcess/HDR.hlsl

Consequently, you should replace/overwrite those files from this repo to see the effect. Also, you must delete the Shaders folder located in

\AppData\Local\Lockheed Martin\Prepar3D v2\

Original files are by LM. Tweaks/modifications are posted here for verification and educational purposes only. I am not responsible if your sim breaks!

If you're reading this, feel free to clone/fork the git!